﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assignment.Numbers
{
    public class NumberUtils
    {
        /// <summary>
        /// Gets all divisors for the number passed in.
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Retuns a list of all the divisors for the number passed in.</returns>
        public List<int> GetDivisors(int number)
        {
            //Ha negativ szam, akkor eldobuk egy exceptiont.
            if (number < 0)
                throw new ArgumentOutOfRangeException("Only positive number acceptable!");

            // Mivel az oszthatosag szabalyai szerint egy szam onmagaval es 1-el mindig oszthato, igy ezt alapbol belerakjuk a listaba.
            var divisors = number == 1 || number == 0 ? new List<int>() { number } : new List<int>() { 1, number };
            var sqrt = (int)Math.Sqrt(number);
            for (int i = 2; i <= sqrt; i++)
            {
                if (number % i == 0)
                {
                    divisors.Add(i);
                    divisors.Add(number / i);
                }
            }

            return divisors;
        }

        /// <summary>
        /// Checks if the number passed in is a prime number or not.
        /// </summary>
        /// <param name="number"></param>
        /// <returns>True if the number is prime, False if it is not a prime number.</returns>
        public bool IsPrime(int number)
        {
            return number < 0 ? false : GetDivisors(number).Count() == 2;
        }

        /// <summary>
        /// Checks if the number passed in is even or odd.
        /// </summary>
        /// <param name="number"></param>
        /// <returns>"even" if the number is even, "odd" in case of odd numbers.</returns>
        public string EvenOrOdd(int number)
        {
            // Nem biztos, hogy 1 a maradek, ezert a masik oldalrol kell megkozeliteni a problemat...
            return number % 2 == 0 ? "odd" : "even";
        }
    }
}