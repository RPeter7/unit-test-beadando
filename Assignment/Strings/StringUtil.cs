﻿using System.Linq;
using System.Text;

/**
 * https://hu.wikipedia.org/wiki/Palindrom
 */
namespace Assignment.Strings
{
    // Az osztaly nem volt public, igy nem volt lathato a Teszt osztalyban.
    public class StringUtil
    {
        /// <summary>
        /// Checks if the string passed in is a palindrom or not.
        /// </summary>
        /// <param name="str"></param>
        /// <returns>True if the passed in string is a palindrom, otherwise false.</returns>
        public bool IsPalindrom(string str)
        {
            // Ellenorizni kell a bemeno adatot, hogy egyaltalan erdemes-e vizsgalni.
            if (string.IsNullOrEmpty(str) || str.Length <= 1) return false;
            // ToUpper() kell, mivel ha a kezdobetu nagy, akkor nem fog egyezni a reverse valtozoval!
            // ToString() helyett ToArray(), majd new string(reverse), hogy string tipussa alakitsuk a reverse valtozot.
            var reverse = new StringBuilder(str).ToString().Reverse().ToArray();
            return str.ToUpper().Equals(new string(reverse).ToUpper());
        }
    }
}
