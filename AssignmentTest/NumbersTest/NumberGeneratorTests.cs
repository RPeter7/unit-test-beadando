﻿using System;
using Assignment.Numbers;
using NUnit.Framework;

namespace AssignmentTest.NumbersTest
{
    [TestFixture]
    public class NumberGeneratorTests
    {
        private NumberGenerator _numberGenerator;

        [OneTimeSetUp]
        public void Setup()
        {
            _numberGenerator = new NumberGenerator();
        }

        [TestCase(-5)]
        [TestCase(0)]
        public void GenerateEven_ShouldThrowArgumentOutOfRangeException_WhenLimitIsNegativeNumber(int testNegativeNumber)
        {
            // Assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => _numberGenerator.GenerateEven(testNegativeNumber));
        }

        [Test]
        public void GenerateEven_ShouldReturnAnEvenNumber()
        {
            // Arrange
            var testLimit = 111;

            // Act
            var result = _numberGenerator.GenerateEven(testLimit);

            // Assert
            Assert.That(result % 2 == 0);
        }

        [TestCase(11)]
        [TestCase(101)]
        public void GenerateEven_ShouldReturnANumberInARangeOfZeroAndGiven_WhenLimitIsGivenInTheParameter(int testLimit)
        {
            // Act
            var resultEven = _numberGenerator.GenerateEven(testLimit);

            // Assert
            Assert.That(resultEven < testLimit && resultEven >= 0);
        }

        [TestCase(-5)]
        [TestCase(0)]
        public void GenerateOdd_ShouldThrowArgumentOutOfRangeException_WhenLimitLessThanOne(int testNegativeNumber)
        {
            // Assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => _numberGenerator.GenerateEven(testNegativeNumber));
        }

        [Test]
        public void GenerateOdd_ShouldReturnAnOddNumber()
        {
            // Arrange
            const int testLimit = 200;

            // Act
            var result = _numberGenerator.GenerateOdd(testLimit);

            // Assert
            Assert.That(result % 2 != 0);
        }

        [TestCase(22)]
        [TestCase(999)]
        public void GenerateOdd_ShouldReturnANumberInARangeOfZeroAndGiven_WhenLimitIsGivenInTheParameter(int testLimit)
        {
            // Act
            var resultOdd = _numberGenerator.GenerateOdd(testLimit);

            // Assert
            Assert.That(resultOdd < testLimit && resultOdd >= 0);
        }
    }
}
