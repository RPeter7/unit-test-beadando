﻿using System;
using System.Collections.Generic;
using Assignment.Numbers;
using NUnit.Framework;

namespace AssignmentTest.NumbersTest
{
    [TestFixture]
    public class NumberUtilsTest
    {
        private NumberUtils _numberUtils;

        [OneTimeSetUp]
        public void Setup()
        {
            _numberUtils = new NumberUtils();
        }

        [Test]
        public void GetDivisors_ShouldReturnAllDivisorOfGivenNumber_WhenNumberIsTwelve()
        {
            // Arrange 
            const int testNumber = 12;

            // Act
            var result = _numberUtils.GetDivisors(testNumber);

            // Assert
            Assert.That(result, Is.EquivalentTo(new List<int>() { 1, 2, 3, 4, 6, testNumber }));
        }

        [TestCase(1)]
        [TestCase(0)]
        public void GetDivisors_ShouldReturnListWithOnlyTheGivenNumber_WhenNumberIsOneOrZero(int testNumber)
        {
            // Act
            var result = _numberUtils.GetDivisors(testNumber);

            // Assert
            Assert.That(result, Is.EquivalentTo(new List<int>() { testNumber }));
        }

        [Test]
        public void GetDivisors_ShouldThrowException_WhenNumberIsNegative()
        {
            // Arrange 
            const int testNumber = -1;

            // Assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => _numberUtils.GetDivisors(testNumber));
        }

        [Test]
        public void IsPrime_ShouldReturnFalse_WhenNumberIsNegative()
        {
            // Arrange
            const int testNumber = -1;

            // Act
            var result = _numberUtils.IsPrime(testNumber);

            // Assert
            Assert.That(result, Is.False);
        }

        [TestCase(6)]
        [TestCase(122)]
        public void IsPrime_ShouldReturnFalse_WhenNumberIsNotPrime(int testNumber)
        {
            // Act
            var result = _numberUtils.IsPrime(testNumber);

            // Assert
            Assert.That(result, Is.False);
        }

        [TestCase(7)]
        [TestCase(113)]
        public void IsPrime_ShouldReturnTrue_WhenNumberIsPrime(int testNumber)
        {
            // Act
            var result = _numberUtils.IsPrime(testNumber);

            // Assert
            Assert.That(result, Is.True);
        }

        [TestCase(7)]
        [TestCase(-21)]
        public void EvenOrOdd_ShouldReturnEven_WhenGivenNumberIsEven(int testNumber)
        {
            // Act
            var result = _numberUtils.EvenOrOdd(testNumber);

            // Assert
            Assert.That(result, Is.EqualTo("even"));
        }

        [TestCase(-6)]
        [TestCase(28)]
        public void EvenOrOdd_ShouldReturnOdd_WhenGivenNumberIsOdd(int testNumber)
        {
            // Act
            var result = _numberUtils.EvenOrOdd(testNumber);

            // Assert
            Assert.That(result, Is.EqualTo("odd"));
        }
    }
}
