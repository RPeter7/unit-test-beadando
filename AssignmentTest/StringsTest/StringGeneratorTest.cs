﻿using System.Collections.Generic;
using Assignment.Numbers;
using Assignment.Strings;
using Moq;
using NUnit.Framework;

namespace AssignmentTest.StringsTest
{
    [TestFixture]
    public class StringGeneratorTest
    {
        private Mock<INumberGenerator> _mockedNumberGenerator;
        private StringGenerator _stringGenerator;

        [OneTimeSetUp]
        public void Setup()
        {
            _mockedNumberGenerator = new Mock<INumberGenerator>();

            _mockedNumberGenerator.Setup(x => x.GenerateEven(It.IsAny<int>())).Returns(4);
            _mockedNumberGenerator.Setup(x => x.GenerateOdd(It.IsAny<int>())).Returns(7);

            _stringGenerator = new StringGenerator(_mockedNumberGenerator.Object);
        }

        [Test]
        public void GenerateEvenOddPairs_ShouldReturnTwoEvenOddPairs_WhenPairCountIsTwo()
        {
            // Arrange
            const int testPairCount = 2;

            // Act
            var result = _stringGenerator.GenerateEvenOddPairs(testPairCount, It.IsAny<int>());

            // Assert
            Assert.That(result, Is.EquivalentTo(new List<string>() { "4,7", "4,7"}));
            Assert.That(result, Has.Exactly(2).Items);
        }

        [TestCase(-2)]
        [TestCase(0)]
        public void GenerateEvenOddPairs_ShouldReturnEmptyList_WhenPairCountIsZeroOrNegative(int testPairCount)
        {
            // Act
            var result = _stringGenerator.GenerateEvenOddPairs(testPairCount, It.IsAny<int>());

            // Assert
            Assert.That(result, Is.Empty);
            Assert.That(result, Has.Exactly(0).Items);
        }
    }
}
