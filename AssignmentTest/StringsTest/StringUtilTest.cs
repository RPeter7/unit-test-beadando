﻿using Assignment.Strings;
using NUnit.Framework;

namespace AssignmentTest.StringsTest
{
    [TestFixture]
    public class StringUtilTest
    {
        private StringUtil _stringUtil;

        [OneTimeSetUp]
        public void Setup()
        {
            _stringUtil = new StringUtil();
        }

        [TestCase("Something")]
        [TestCase("Another")]
        public void IsPalindrom_ShouldReturnFalse_WhenStrIsNotPalindrom(string testStr)
        {
            // Act
            var result = _stringUtil.IsPalindrom(testStr);

            // Assert
            Assert.That(result, Is.False);
        }

        [TestCase("Hannah")]
        [TestCase("Repaper")]
        public void IsPalindrom_ShouldReturnTrue_WhenStrIsPalindrom(string testStr)
        {
            // Act
            var result = _stringUtil.IsPalindrom(testStr);

            // Assert
            Assert.That(result, Is.True);
        }

        [TestCase("")]
        [TestCase(null)]
        [TestCase("a")]
        public void IsPalindrom_ShouldReturnFalse_WhenParameterIsEmptyOrNullOrLengthIsOne(string testStr)
        {
            // Act
            var result = _stringUtil.IsPalindrom(testStr);

            // Assert
            Assert.That(result, Is.False);
        }
    }
}
